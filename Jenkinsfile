@Library('jenkins-shared-library') _
def gv

pipeline {
    agent any
    tools {
        maven 'maven-3.6.3'
    }
    environment {
        // env.VERSION will be created by npmParseVersion() script
        REPO_URL = "gitlab.com/fieryybird/bootcamp-aws-demo-java-maven.git"
        GIT_CREDENTIALS_ID = 'gitlab-credentials'
        IMAGE = "fieryybird/java-app"

    }
    stages {
        stage("increment app version") {
            when {
                expression { BRANCH_NAME == 'main' }
            }
            steps {
                script {
                    sh 'echo "Incrementing app version..."'
                    mavenVersionUpdateMinor()
                    mavenParseVersion()
//                    def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
//                    def extract_vers = matcher[0][1]
//                    env.VERSION = "$extract_vers"
                }
            }
        }
        stage("build jar file") {
            when {
                expression { BRANCH_NAME == 'main' }
            }
            steps {
                mavenBuild()
            }
        }
        stage("build and push image") {
            when {
                expression { BRANCH_NAME == 'main' }
            }
            steps {
                script {
                    dockerHubLogin()
                    dockerBuild "${env.IMAGE}:${env.VERSION}"
                    dockerPush "${env.IMAGE}:${env.VERSION}"
                }
            }
        }
        stage("commit version update") {
            when {
                expression { BRANCH_NAME == 'main' }
            }
            steps {
                script {
                    gitConfigJenkinsUser()
                    gitSetRepository("${env.GIT_CREDENTIALS_ID}", "${env.REPO_URL}")
                    gitCommitVersionUpdate("${env.VERSION}")
                }
            }
        }
        stage("server provisioning") {
            when {
                expression { BRANCH_NAME == 'main' }
            }
            environment {
                AWS_ACCESS_KEY_ID = credentials('aws-admin-access-key')
                AWS_SECRET_ACCESS_KEY = credentials('aws-admin-secret-key')
                TF_VAR_tags='{"name":"java-maven-app","env":"test"}'

            }
            steps {
                script {
                    dir('terraform') {
                        sh "terraform init"
                        sh "terraform apply --auto-approve"
                        EC2_PUBLIC_IP = sh(
                            script: "terraform output ec2_public_ip",
                            returnStdout: true
                        ).trim()
                    }
                }
            }            
        }
        stage("deploy-aws") {
            when {
                expression { BRANCH_NAME == 'main' }
            }
            environment {
                DOCKER_CREDS = credentials('dockerhub-credentials')
            }
            steps {
                script {
                    echo "waiting for EC2 server ${EC2_PUBLIC_IP} to initialize"
                    sleep(time:90, unit: "SECONDS")
                //    def dockerCmd = "docker run -p 8080:8080 -d 37.187.124.95:8888/demojavapp:${env.VERSION}"
                    def runScript = "bash ./server-commands.sh ${env.VERSION} ${env.DOCKER_CREDS_USR} ${env.DOCKER_CREDS_PSW}"
                    def ec2instance = "ec2-user@${EC2_PUBLIC_IP}"
                    sshagent(['aws-server-ssh-key']) {
                        sh "scp -o StrictHostKeyChecking=no server-commands.sh ${ec2instance}:/home/ec2-user"
                        sh "scp -o StrictHostKeyChecking=no docker-compose.yaml ${ec2instance}:/home/ec2-user"
                        sh "ssh -o StrictHostKeyChecking=no ${ec2instance} ${runScript}"
                    }
                }
            }
        }
    }
    post {
        always {
            sh 'echo "Pipeline was successful. GJ! Cleaning up..."'
            sh "docker rmi fieryybird/java-app:${VERSION}"
            cleanWs()
        }
    }
}
