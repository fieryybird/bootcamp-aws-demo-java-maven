FROM openjdk:8-jre-alpine

EXPOSE 8080
WORKDIR /app

COPY ./target/java-maven-app-*.jar .

CMD java -jar java-maven-app-*.jar
