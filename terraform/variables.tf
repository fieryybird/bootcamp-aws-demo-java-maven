variable "availability_zone" {
  type = string
  default = "us-east-1a"
}

variable "instance_type" {
  type = string
  default = "t2.micro"
}

variable "entry_script" {
  type = string
  default = "entry_script.sh"
}

variable "vpc_cidr_block" {
  type = string
  default = "10.0.0.0/16"
}

variable "subnet_cidr_block" {
  type = string
  default = "10.0.10.0/24"
}

variable "tags" {
  type = map(any)
  default = {
  name = "java-maven-app"
  env  = "dev"}
}

variable "sg_allowed_ports" {
  type = list(string)
  default = ["22", "80", "443", "8080"]
}


