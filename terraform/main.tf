resource "aws_vpc" "demo_vpc" {
  cidr_block = var.vpc_cidr_block
  tags = {
    Name = "${var.tags.name}-vpc"
  }
}

resource "aws_subnet" "demo_subnet" {
  vpc_id            = aws_vpc.demo_vpc.id
  cidr_block        = var.subnet_cidr_block
  availability_zone = var.availability_zone
  tags = {
    Name = "${var.tags.name}-subnet"
  }
}

resource "aws_internet_gateway" "demo_igw" {
  vpc_id = aws_vpc.demo_vpc.id
  tags = {
    Name = "${var.tags.name}-igw"
  }
}

resource "aws_default_route_table" "demo_rtb" {
  default_route_table_id = aws_vpc.demo_vpc.default_route_table_id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.demo_igw.id
  }
  tags = {
    Name = "${var.tags.name}-rtb"
  }
}

resource "aws_default_security_group" "demo_sg" {
  vpc_id = aws_vpc.demo_vpc.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.tags.name}-sg"
  }
}

resource "aws_security_group_rule" "demo_sg_ingress_rules" {
  security_group_id = aws_default_security_group.demo_sg.id
  type              = "ingress"
  protocol          = "tcp"

  for_each    = toset(var.sg_allowed_ports)
  from_port   = each.value
  to_port     = each.value
  cidr_blocks = ["0.0.0.0/0"]
}

data "aws_ami" "latest_amazon_linux_image" {
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "name"
    values = ["al2023-ami-*-x86_64"]
  }
}

resource "aws_instance" "demo_ec2" {
  availability_zone = var.availability_zone
  ami               = data.aws_ami.latest_amazon_linux_image.id
  instance_type     = var.instance_type

  subnet_id                   = aws_subnet.demo_subnet.id
  vpc_security_group_ids      = [aws_default_security_group.demo_sg.id]
  associate_public_ip_address = true

  key_name = "server-ssh-key"
  user_data = file(var.entry_script) 

  tags = {
    Name = "${var.tags.name}-server"
  }
}