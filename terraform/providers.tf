terraform {
  backend "s3" {
    bucket = "fieryybird-terraform-states"
    key = "java-maven-app/main.tfstate"
    region = "us-east-1"
  }
}

provider "aws" {
  region = "us-east-1"
}